<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use App\Entity\Author;
use App\Entity\Book;

class AuthorTest extends TestCase
{


    public function testGetBooksReturnsEmptyCollectionWhenNoBooksExist()
    {
        $author = new Author();
        $this->assertInstanceOf(Collection::class, $author->getBooks());
        $this->assertCount(0, $author->getBooks());
    }

    public function testName(): void
    {
        $author = new Author();
        $author->setName('John Doe');
        $this->assertSame('John Doe', $author->getName());
    }


    public function testAddBookAddsBookToAuthorAndAuthorToBook()
    {
        $author = new Author();
        $book = new Book();

        $author->addBook($book);


        $this->assertCount(1, $author->getBooks());
        $this->assertTrue($author->getBooks()->contains($book));
        $this->assertTrue($book->getAuthors()->contains($author));
    }

    public function testRemoveBookRemovesBookFromAuthorAndAuthorFromBook()
    {
        $author = new Author();
        $book = new Book();

        $author->addBook($book);
        $author->removeBook($book);

        $this->assertCount(0, $author->getBooks());
        $this->assertFalse($author->getBooks()->contains($book));
        $this->assertFalse($book->getAuthors()->contains($author));
    }

    public function testAddBookDoesNotDuplicateBookInAuthor()
    {
        $author = new Author();
        $book = new Book();

        $author->addBook($book);
        $author->addBook($book);

        $this->assertCount(1, $author->getBooks());
    }
}
