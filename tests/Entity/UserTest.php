<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGetUserBooksReturnsEmptyCollectionByDefault()
    {
        $user = new User();
        $userBooks = $user->getUserBooks();

        $this->assertInstanceOf(Collection::class, $userBooks);
        $this->assertEmpty($userBooks);
    }

    public function testAddUserBookAddsUserBookAndSetsReader()
    {
        $user = new User();
        $userBook = new UserBook();

        $user->addUserBook($userBook);
        $userBooks = $user->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($user, $userBook->getReader());
    }

    public function testRemoveUserBookRemovesUserBookAndUnsetsReader()
    {
        $user = new User();
        $userBook = new UserBook();

        $user->addUserBook($userBook);
        $user->removeUserBook($userBook);
        $userBooks = $user->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getReader());
    }

    public function testGetRolesReturnsDefaultRoleUser()
    {
        $user = new User();
        $roles = $user->getRoles();

        $this->assertIsArray($roles);
        $this->assertContains('ROLE_USER', $roles);
    }

    public function testGetRolesReturnsUniqueRoles()
    {
        $user = new User();
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER', 'ROLE_ADMIN']);

        $roles = $user->getRoles();

        $this->assertIsArray($roles);
        $this->assertCount(2, $roles);
        $this->assertContains('ROLE_ADMIN', $roles);
        $this->assertContains('ROLE_USER', $roles);
    }

    public function testGetPasswordReturnsPassword()
    {
        $user = new User();
        $user->setPassword('password');

        $this->assertSame('password', $user->getPassword());
    }

    public function testGetUsernameReturnsEmail()
    {
        $user = new User();
        $user->setEmail('email@example.com');
        $this->assertSame('email@example.com', $user->getEmail());
    }

    public function testGetUsernameReturnsPseudo()
    {
        $user = new User();
        $user->setPseudo('pseudo');
        $this->assertSame('pseudo', $user->getPseudo());
    }
    //test getUserIdentifier
    public function testGetUserIdentifierReturnsEmail()
    {
        $user = new User();
        $user->setEmail('email@test.com');
        $this->assertSame('email@test.com', $user->getUserIdentifier());
    }

    public function testRemoveUserBook()
    {
        $user = new User();
        $userBook = $this->createMock(UserBook::class);
        $user->addUserBook($userBook);
        $user->removeUserBook($userBook);
        $this->assertFalse($user->getUserBooks()->contains($userBook));
    }

    public function testGetId()
    {
        $user = new User();
        $this->assertNull($user->getId());
    }

}
