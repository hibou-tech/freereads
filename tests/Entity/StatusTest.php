<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testGetUserBooksReturnsEmptyCollectionByDefault()
    {
        $status = new Status();
        $userBooks = $status->getUserBooks();

        $this->assertInstanceOf(Collection::class, $userBooks);
        $this->assertEmpty($userBooks);
    }

    public function testAddUserBookAddsUserBookAndSetsStatus()
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);
        $userBooks = $status->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());
    }

    public function testRemoveUserBookRemovesUserBookAndUnsetsStatus()
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);
        $status->removeUserBook($userBook);
        $userBooks = $status->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getStatus());
    }
}
