<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use App\Entity\Status;

class UserBookTest extends TestCase
{
    public function testGetIdReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getId());
    }

    public function testGetCreatedAtReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getCreatedAt());
    }

    public function testSetCreatedAtSetsCreatedAt()
    {
        $userBook = new UserBook();
        $createdAt = new \DateTimeImmutable();

        $userBook->setCreatedAt($createdAt);

        $this->assertSame($createdAt, $userBook->getCreatedAt());
    }

    public function testGetUpdatedAtReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getUpdatedAt());
    }

    public function testSetUpdatedAtSetsUpdatedAt()
    {
        $userBook = new UserBook();
        $updatedAt = new \DateTimeImmutable();

        $userBook->setUpdatedAt($updatedAt);

        $this->assertSame($updatedAt, $userBook->getUpdatedAt());
    }

    public function testGetCommentReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getComment());
    }

    public function testSetCommentSetsComment()
    {
        $userBook = new UserBook();
        $comment = "This is a comment";

        $userBook->setComment($comment);

        $this->assertSame($comment, $userBook->getComment());
    }

    public function testGetRatingReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getRating());
    }

    public function testSetRatingSetsRating()
    {
        $userBook = new UserBook();
        $rating = 5;

        $userBook->setRating($rating);

        $this->assertSame($rating, $userBook->getRating());
    }

    public function testGetReaderReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getReader());
    }

    public function testSetReaderSetsReader()
    {
        $userBook = new UserBook();
        $reader = new User();

        $userBook->setReader($reader);

        $this->assertSame($reader, $userBook->getReader());
    }

    public function testGetBookReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getBook());
    }

    public function testSetBookSetsBook()
    {
        $userBook = new UserBook();
        $book = new Book();

        $userBook->setBook($book);

        $this->assertSame($book, $userBook->getBook());
    }

    public function testGetStatusReturnsNullByDefault()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getStatus());
    }

    public function testSetStatusSetsStatus()
    {
        $userBook = new UserBook();
        $status = new Status();

        $userBook->setStatus($status);

        $this->assertSame($status, $userBook->getStatus());
    }
}
