<?php

declare(strict_types=1);

namespace App\Tests\Entity;


use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Publisher;
use App\Entity\UserBook;


class BookTest extends TestCase
{
    public function testGetIdReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getId());
    }

    public function testGetGoogleBooksIdReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getGoogleBooksId());
    }

    public function testSetGoogleBooksIdSetsGoogleBooksId()
    {
        $book = new Book();
        $googleBooksId = "123456789";

        $book->setGoogleBooksId($googleBooksId);

        $this->assertSame($googleBooksId, $book->getGoogleBooksId());
    }

    public function testGetTitleReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getTitle());
    }

    public function testSetTitleSetsTitle()
    {
        $book = new Book();
        $title = "Book Title";

        $book->setTitle($title);

        $this->assertSame($title, $book->getTitle());
    }

    public function testGetSubtitleReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getSubtitle());
    }

    public function testSetSubtitleSetsSubtitle()
    {
        $book = new Book();
        $subtitle = "Book Subtitle";

        $book->setSubtitle($subtitle);

        $this->assertSame($subtitle, $book->getSubtitle());
    }

    public function testGetPublishDateReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getPublishDate());
    }

    public function testSetPublishDateSetsPublishDate()
    {
        $book = new Book();
        $publishDate = new \DateTime();

        $book->setPublishDate($publishDate);

        $this->assertSame($publishDate, $book->getPublishDate());
    }

    public function testGetDescriptionReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getDescription());
    }

    public function testSetDescriptionSetsDescription()
    {
        $book = new Book();
        $description = "Book description";

        $book->setDescription($description);

        $this->assertSame($description, $book->getDescription());
    }

    public function testGetIsbn10ReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getIsbn10());
    }

    public function testSetIsbn10SetsIsbn10()
    {
        $book = new Book();
        $isbn10 = "1234567890";

        $book->setIsbn10($isbn10);

        $this->assertSame($isbn10, $book->getIsbn10());
    }

    public function testGetIsbn13ReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getIsbn13());
    }

    public function testSetIsbn13SetsIsbn13()
    {
        $book = new Book();
        $isbn13 = "1234567890123";

        $book->setIsbn13($isbn13);

        $this->assertSame($isbn13, $book->getIsbn13());
    }

    public function testGetPageCountReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getPageCount());
    }

    public function testSetPageCountSetsPageCount()
    {
        $book = new Book();
        $pageCount = 200;

        $book->setPageCount($pageCount);

        $this->assertSame($pageCount, $book->getPageCount());
    }

    public function testGetSmallThumbnailReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getSmallThumbnail());
    }

    public function testSetSmallThumbnailSetsSmallThumbnail()
    {
        $book = new Book();
        $smallThumbnail = "path/to/small_thumbnail.jpg";

        $book->setSmallThumbnail($smallThumbnail);

        $this->assertSame($smallThumbnail, $book->getSmallThumbnail());
    }

    public function testGetThumbnailReturnsNullByDefault()
    {
        $book = new Book();
        $this->assertNull($book->getThumbnail());
    }

    public function testSetThumbnailSetsThumbnail()
    {
        $book = new Book();
        $thumbnail = "path/to/thumbnail.jpg";

        $book->setThumbnail($thumbnail);

        $this->assertSame($thumbnail, $book->getThumbnail());
    }

    public function testGetAuthorsReturnsEmptyCollectionByDefault()
    {
        $book = new Book();
        $authors = $book->getAuthors();

        $this->assertInstanceOf(Collection::class, $authors);
        $this->assertEmpty($authors);
    }

    public function testAddAuthorAddsAuthor()
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);
        $authors = $book->getAuthors();

        $this->assertCount(1, $authors);
        $this->assertTrue($authors->contains($author));
    }

    public function testRemoveAuthorRemovesAuthor()
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);
        $book->removeAuthor($author);
        $authors = $book->getAuthors();

        $this->assertCount(0, $authors);
        $this->assertFalse($authors->contains($author));
    }

    public function testGetPublishersReturnsEmptyCollectionByDefault()
    {
        $book = new Book();
        $publishers = $book->getPublishers();

        $this->assertInstanceOf(Collection::class, $publishers);
        $this->assertEmpty($publishers);
    }

    public function testAddPublisherAddsPublisher()
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);
        $publishers = $book->getPublishers();

        $this->assertCount(1, $publishers);
        $this->assertTrue($publishers->contains($publisher));
    }

    public function testRemovePublisherRemovesPublisher()
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);
        $book->removePublisher($publisher);
        $publishers = $book->getPublishers();

        $this->assertCount(0, $publishers);
        $this->assertFalse($publishers->contains($publisher));
    }

    public function testGetUserBooksReturnsEmptyCollectionByDefault()
    {
        $book = new Book();
        $userBooks = $book->getUserBooks();

        $this->assertInstanceOf(Collection::class, $userBooks);
        $this->assertEmpty($userBooks);
    }

    public function testAddUserBookAddsUserBookAndSetsBook()
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);
        $userBooks = $book->getUserBooks();

        $this->assertCount(1, $userBooks);
        $this->assertTrue($userBooks->contains($userBook));
        $this->assertSame($book, $userBook->getBook());
    }

    public function testRemoveUserBookRemovesUserBookAndUnsetsBook()
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);
        $book->removeUserBook($userBook);
        $userBooks = $book->getUserBooks();

        $this->assertCount(0, $userBooks);
        $this->assertFalse($userBooks->contains($userBook));
        $this->assertNull($userBook->getBook());
    }
}
