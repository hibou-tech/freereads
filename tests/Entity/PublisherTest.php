<?php

namespace App\Tests\Entity;

use App\Entity\Publisher;
use App\Entity\Book;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testGetBooksReturnsEmptyCollectionByDefault()
    {
        $publisher = new Publisher();
        $books = $publisher->getBooks();

        $this->assertInstanceOf(Collection::class, $books);
        $this->assertEmpty($books);
    }

    public function testAddBookAddsBookAndSetsPublisher()
    {
        $publisher = new Publisher();
        $book = new Book();

        $publisher->addBook($book);
        $books = $publisher->getBooks();

        $this->assertCount(1, $books);
        $this->assertTrue($books->contains($book));
        $this->assertSame($publisher, $book->getPublishers()->first());
    }

    public function testRemoveBookRemovesBookAndUnsetsPublisher()
    {
        $publisher = new Publisher();
        $book = new Book();

        $publisher->addBook($book);
        $publisher->removeBook($book);
        $books = $publisher->getBooks();

        $this->assertCount(0, $books);
        $this->assertFalse($books->contains($book));
        $this->assertEmpty($book->getPublishers());
    }
}
