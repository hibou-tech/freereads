<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $authros = [];

        for ($i = 0; $i < 10; ++$i) {
            $author = new Author();
            $author->setName($faker->name());

            $manager->persist($author);
            $authors[] = $author;
        }
        // create 10 publishers
        $publishers = [];
        for ($i = 0; $i < 10; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->company());

            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // create 3 status read, to read, reading
        $statuses = [];
        foreach (['read', 'to-read', 'reading'] as $status) {
            $oneStatus = new Status();
            $oneStatus->setName($status);

            $manager->persist($oneStatus);
            $statuses[] = $oneStatus;
        }
        // create 100 books

        $books = [];
        for ($i = 0; $i < 100; ++$i) {
            $book = new Book();
            $book->setTitle($faker->sentence(3))
                ->setSubtitle($faker->sentence(6))
                ->setGoogleBooksId($faker->uuid())
                ->setPublishDate($faker->dateTimeBetween('-100 years', 'now'))
                ->setDescription($faker->paragraph(3))
                ->setIsbn10($faker->isbn10())
                ->setIsbn13($faker->isbn13())
                ->setPageCount($faker->numberBetween(10, 1000))
                ->setSmallThumbnail($faker->imageUrl(100, 100))
                ->setThumbnail($faker->imageUrl(200, 200))
                ->addAuthor($faker->randomElement($authors))
                ->addPublisher($faker->randomElement($publishers));

            $manager->persist($book);

            $books[] = $book;
        }

        // create 10 users
        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $user->setEmail($faker->email())
                ->setPassword($faker->password())
                ->setPseudo($faker->userName);

            $manager->persist($user);

            $users[] = $user;
        }

        // create 10 userBooks  for each user
        foreach ($users as $user) {
            for ($i = 0; $i < 10; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setReader($user)
                    ->setBook($faker->randomElement($books))
                    ->setStatus($faker->randomElement($statuses))
                    ->setComment($faker->paragraph(3))
                    ->setRating($faker->numberBetween(0, 5))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime))
                    ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));

                $manager->persist($userBook);
            }
        }

        $manager->flush();
    }
}
